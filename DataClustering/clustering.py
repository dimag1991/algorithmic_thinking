"""
Template for Project 3
Student will implement four functions:

slow_closest_pairs(cluster_list)
fast_closest_pair(cluster_list) - implement fast_helper()
hierarchical_clustering(cluster_list, num_clusters)
kmeans_clustering(cluster_list, num_clusters, num_iterations)

where cluster_list is a list of clusters in the plane
"""

import math
import alg_cluster
import random
import time
import matplotlib.pyplot as plt
import urllib2


DIRECTORY = "http://commondatastorage.googleapis.com/codeskulptor-assets/"
DATA_3108_URL = DIRECTORY + "data_clustering/unifiedCancerData_3108.csv"
DATA_896_URL = DIRECTORY + "data_clustering/unifiedCancerData_896.csv"
DATA_290_URL = DIRECTORY + "data_clustering/unifiedCancerData_290.csv"
DATA_111_URL = DIRECTORY + "data_clustering/unifiedCancerData_111.csv"


def load_data_table(data_url):
    """
    Import a table of county-based cancer risk data
    from a csv format file
    """
    data_file = urllib2.urlopen(data_url)
    data = data_file.read()
    data_lines = data.split('\n')
    print "Loaded", len(data_lines), "data points"
    data_tokens = [line.split(',') for line in data_lines]
    return [[tokens[0], float(tokens[1]), float(tokens[2]), int(tokens[3]), float(tokens[4])] 
            for tokens in data_tokens]


def pair_distance(cluster_list, idx1, idx2):
    """
    Helper function to compute Euclidean distance between two clusters
    in cluster_list with indices idx1 and idx2
    
    Returns tuple (dist, idx1, idx2) with idx1 < idx2 where dist is distance between
    cluster_list[idx1] and cluster_list[idx2]
    """
    return (cluster_list[idx1].distance(cluster_list[idx2]), idx1, idx2)


def slow_closest_pair(clusters):
    """
    returns a closest pair
    """
    (closest_dist, idx, idy) = (float("inf"), -1, -1)
    cluster_number = len(clusters)
    for id1 in range(0, cluster_number):
        for id2 in range(id1+1, cluster_number):
            dist = pair_distance(clusters, id1, id2)
            if dist[0] < closest_dist:
                (closest_dist, idx, idy) = dist
    return (closest_dist, idx, idy)           


def slow_closest_pairs(cluster_list):
    """
    Compute the set of closest pairs of cluster in list of clusters
    using O(n^2) all pairs algorithm
    
    Returns the set of all tuples of the form (dist, idx1, idx2) 
    where the cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.   
    
    """
    all_closest_pairs = set()
    closest_distance = float("inf")
    cluster_number = len(cluster_list)
    for id1 in range(0, cluster_number):
        for id2 in range(id1+1, cluster_number):
           dist = pair_distance(cluster_list, id1, id2)
           if dist[0] < closest_distance:
               closest_distance = dist[0]
               all_closest_pairs = set()
               all_closest_pairs.add(dist)
           if dist[0] == closest_distance:
               all_closest_pairs.add(dist)
    return all_closest_pairs


def fast_closest_pair(cluster_list):
    """
    Compute a closest pair of clusters in cluster_list
    using O(n log(n)) divide and conquer algorithm
    
    Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
    cluster_list[idx1] and cluster_list[idx2]
    have the smallest distance dist of any pair of clusters
    """
        
    def fast_helper(cluster_list, horiz_order, vert_order):
        """
        Divide and conquer method for computing distance between closest pair of points
        Running time is O(n * log(n))
        
        horiz_order and vert_order are lists of indices for clusters
        ordered horizontally and vertically
        
        Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
        cluster_list[idx1] and cluster_list[idx2]
        have the smallest distance dist of any pair of clusters
    
        """
        
        # base case
        cluster_num = len(horiz_order)
        if cluster_num <= 3:
            closest_pair = slow_closest_pair([cluster_list[horiz_order[cluster_id]] for cluster_id in range(0, cluster_num)])
            return (closest_pair[0], horiz_order[closest_pair[1]], horiz_order[closest_pair[2]]) 
        # divide
        middle = int(math.ceil(cluster_num/2))
        dividing_line_coord = (cluster_list[horiz_order[middle-1]].horiz_center() + cluster_list[horiz_order[middle]].horiz_center())/2
        horiz_order_left_list = horiz_order[:middle]
        horiz_order_right_list = horiz_order[middle:]
        horiz_order_left_set = set(horiz_order_left_list)
        horiz_order_right_set = set(horiz_order_right_list)
        vert_order_left = [vert_order[cluster_id] for cluster_id in range(0, len(vert_order)) if vert_order[cluster_id] in horiz_order_left_set]
        vert_order_right = [vert_order[cluster_id] for cluster_id in range(0, len(vert_order)) if vert_order[cluster_id] in horiz_order_right_set]
        left_answer =  fast_helper(cluster_list, horiz_order_left_list, vert_order_left)
        right_answer = fast_helper(cluster_list, horiz_order_right_list, vert_order_right)
        whole_answer = min(left_answer, right_answer)
        # conquer
        cluster_ids = [vert_order[cluster_id] for cluster_id in range(0, len(vert_order)) if abs(cluster_list[vert_order[cluster_id]].horiz_center() - dividing_line_coord) < whole_answer[0]]
        for cluster_id1 in range(0, len(cluster_ids)-1):
            for cluster_id2 in range(cluster_id1+1, min(cluster_id1 + 3, len(cluster_ids)-1) + 1):
                whole_answer = min(whole_answer, pair_distance(cluster_list, cluster_ids[cluster_id1], cluster_ids[cluster_id2])) 
        return whole_answer
            
    # compute list of indices for the clusters ordered in the horizontal direction
    hcoord_and_index = [(cluster_list[idx].horiz_center(), idx) 
                        for idx in range(len(cluster_list))]    
    hcoord_and_index.sort()
    horiz_order = [hcoord_and_index[idx][1] for idx in range(len(hcoord_and_index))]
     
    # compute list of indices for the clusters ordered in vertical direction
    vcoord_and_index = [(cluster_list[idx].vert_center(), idx) 
                        for idx in range(len(cluster_list))]    
    vcoord_and_index.sort()
    vert_order = [vcoord_and_index[idx][1] for idx in range(len(vcoord_and_index))]

    # compute answer recursively
    answer = fast_helper(cluster_list, horiz_order, vert_order) 
    return (answer[0], min(answer[1:]), max(answer[1:]))

    
def hierarchical_clustering(cluster_list, num_clusters):
    """
    Compute a hierarchical clustering of a set of clusters
    Note: the function mutates cluster_list
    
    Input: List of clusters, number of clusters
    Output: List of clusters whose length is num_clusters
    """
    #i = 0
    while len(cluster_list) > num_clusters:
        (dummy_dist, cluster1_id, cluster2_id) = fast_closest_pair(cluster_list)
        cluster_list[cluster1_id].merge_clusters(cluster_list[cluster2_id])
        cluster_list.remove(cluster_list[cluster2_id])
     #   i = i + 1
      #  if i%10 == 0:
       #     print i
    return cluster_list


def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
    Compute the k-means clustering of a set of clusters
    Note: the function mutates cluster_list
    
    Input: List of clusters, number of clusters, number of iterations
    Output: List of clusters whose length is num_clusters
    """
    
    # initialize k-means clusters to be initial clusters with largest populations 
    centers = [cluster_params[1] for cluster_params in sorted([(point.total_population(), (point.horiz_center(), point.vert_center())) for point in cluster_list], reverse=True)[0:num_clusters]]  
    for dummy_iter_id in range(0, num_iterations):
        clusters = [alg_cluster.Cluster(set([]), center[0], center[1], 0, 0) for center in centers]
        for point in cluster_list:
             (dummy_dist, closest_center) = min([(math.sqrt((centers[center_id][0] - point.horiz_center())**2 + (centers[center_id][1] - point.vert_center())**2), clusters[center_id]) for center_id in range(num_clusters)])        
             closest_center.merge_clusters(point)
        centers = [(cluster.horiz_center(), cluster.vert_center()) for cluster in clusters]     
    return clusters
  
  
def gen_random_clusters(num_clusters):
    """
    generate random points
    """
    return [alg_cluster.Cluster(set([]), 2*random.random() - 1, 2*random.random() - 1, 0, 0) for i in range(num_clusters)]

        
def closest_pair_timing():
    """
    running time analysis for slow_ and fast_ closest pair methods
    """
    slow_times = []
    fast_times = []
    num_clusters = range(2, 201)
    for num in num_clusters:
        points = gen_random_clusters(num)
        begin = time.time()
        slow_closest_pairs(points)
        slow_times.append(time.time() - begin)
        begin = time.time()
        fast_closest_pair(points)
        fast_times.append(time.time() - begin)
    plt.figure()    
    plt.xlabel('number of clusters')
    plt.ylabel('time in seconds')
    plt.plot(num_clusters, slow_times, '-b', label='slow_closest_pairs()')
    plt.plot(num_clusters, fast_times, '-r', label='fast_closest_pair()')
    plt.title('runing time analysis for closest pair problem in Desktop Anaconda Python')
    plt.legend(loc='upper right')
    plt.show()   
    
    
def compute_distortion(cluster_list, data_table):
    """
    computes distortion of a clustering
    """
    distortion = 0
    for cluster in cluster_list:
        distortion += cluster.cluster_error(data_table)
    return distortion
 
   
def quality_analysis(data_table):
    """
    analyze a quality of clustering on a given data set
    """
    hierarchical_distortions = []
    kmeans_distortions = []
    singleton_list = []
    for line in data_table:
        singleton_list.append(alg_cluster.Cluster(set([line[0]]), line[1], line[2], line[3], line[4]))
    for num in range(6, 21):
        cluster_list = kmeans_clustering(singleton_list, num, 5)
        kmeans_distortions.append(compute_distortion(cluster_list, data_table))   
    for num in range(20, 5, -1):
        singleton_list = hierarchical_clustering(singleton_list, num)
        hierarchical_distortions.append(compute_distortion(singleton_list, data_table))   
    plt.figure()    
    plt.xlabel('number of clusters')
    plt.ylabel('distortion')
    plt.plot(range(20, 5, -1), hierarchical_distortions, '-b', label='hirerarchical')
    plt.plot(range(6, 21), kmeans_distortions, '-r', label='kmeans')
    plt.title('clustering quality analysis for ' + str(len(data_table)) + ' data set')
    plt.legend(loc='upper right')
    plt.show()       
    
    
if __name__ == "__main__":
    begin = time.time()
    #closest_pair_timing()
    data_table = load_data_table(DATA_111_URL)
    singleton_list = []
    for line in data_table:
        singleton_list.append(alg_cluster.Cluster(set([line[0]]), line[1], line[2], line[3], line[4]))
    #cluster_list = hierarchical_clustering(singleton_list, 9)
    cluster_list = kmeans_clustering(singleton_list, 9, 5)
    print compute_distortion(cluster_list, data_table)
#    quality_analysis(load_data_table(DATA_111_URL))
#    quality_analysis(load_data_table(DATA_290_URL))
#    quality_analysis(load_data_table(DATA_896_URL))
    print time.time() - begin 

    