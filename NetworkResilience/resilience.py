"""
module to compute the connected components of the graph
"""



from collections import deque
from UPATrial import *
import random
import urllib2
import time
import matplotlib.pyplot as plt

NETWORK_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_rf7.txt"

class UF:
    """    
    class describes on union-find data structure algorithm 
    """
    
    def __init__(self, node_numbers):
        """
        initialize data structure
        """
        self._parent_link_forest = {}
        self._component_sizes = {}
        self._resilience = 1
        for node in node_numbers:
            self._parent_link_forest[node] = node
            self._component_sizes[node] = 1
            
    def find(self, node):
        """
        find node's component number
        """
        while node != self._parent_link_forest[node]:
            node = self._parent_link_forest[node]
        return node 
        
    def union(self, node1, node2):
        """
        form a connection between node1 and node2 if there is no one
        """
        component1 = self.find(node1)
        component2 = self.find(node2)
        if component1 == component2:
            return
        if self._component_sizes[component1] < self._component_sizes[component2]:
            self._parent_link_forest[component1] = component2
            self._component_sizes[component2] = self._component_sizes[component2] + self._component_sizes[component1]
            self._component_sizes.pop(component1)
            if self._component_sizes[component2] > self._resilience:
                self._resilience = self._component_sizes[component2]
        else:
            self._parent_link_forest[component2] = component1
            self._component_sizes[component1] = self._component_sizes[component1] + self._component_sizes[component2]
            self._component_sizes.pop(component2)
            if self._component_sizes[component1] > self._resilience:
                self._resilience = self._component_sizes[component1]
            
    def get_resilience(self):
        """
        get the largest component size
        """
        return self._resilience
        
    def get_component_sizes(self):
        """
        returns component sizes
        """
        return self._component_sizes
        
def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph
    
    Returns a dictionary that models a graph
    """
    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]
    
    print "Loaded graph with", len(graph_lines), "nodes"
    
    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1 : -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph
    
def copy_graph(graph):
    """
    Make a copy of a graph
    """
    new_graph = {}
    for node in graph:
        new_graph[node] = set(graph[node])
    return new_graph    
  
def make_complete_graph(num_nodes):
    """
    returns a complete graph
    """
    graph = {}
    for node in range(0, num_nodes):
        graph[node] = set()
    for node in range(0, num_nodes):
        for neigh in range(node+1, num_nodes):
            graph[node].add(neigh)
            graph[neigh].add(node)
    return graph

def ER(n, p):
    """
    generates a random graph
    """
    random_graph = {}
    for i in range(0, n):
        random_graph[i] = set([])
    for i in range(0, n):
        for j in range(0, n):
            if i != j and random.random() < p:
                random_graph[i].add(j)
                random_graph[j].add(i)
    return random_graph            
    
def UPA(n, m):  
    """
    generates a hierarchical "random graph"
    """
    graph = make_complete_graph(m)
    trial = UPATrial(m)
    for i in range(m, n):
        neighbours = trial.run_trial(m)
        graph[i] = neighbours
        for neigh in neighbours:
            graph[neigh].add(i)
    return graph     

def bfs_visited(ugraph, start_node):
    """
    returns the set of nodes in a connected component with node start_node    
    """
    queue = deque()
    visited = set()
    visited.add(start_node)
    queue.append(start_node)
    while queue:
        curr_node = queue.pop()
        for neigh in ugraph[curr_node]:
            if neigh not in visited:
                visited.add(neigh)
                queue.append(neigh)
    return visited
    
def cc_visited(ugraph):
    """
    returns a list of connected components in the graph
    """
    connected_components = []
    remaining = set()
    for node in ugraph:
        remaining.add(node)
    while remaining:
        curr_node = next(iter(remaining))
        connected_component = bfs_visited(ugraph, curr_node)
        connected_components.append(connected_component) 
        for component_node in connected_component:
            remaining.remove(component_node)
    return connected_components
    
def largest_cc_size(ugraph):
    """
    returns the size of a largest cc in the graph
    """
    connected_components = cc_visited(ugraph)    
    max_size = 0
    for component in connected_components:
        if len(component) > max_size:
            max_size = len(component)
    return max_size     

def compute_resilience(ugraph, attack_order):
    """
    returns a list of largest connected componemts remaining after successive removing
    a list of attack_order "servers" from the network
    """
    ugraph = copy_graph(ugraph)
    resiliencies = [largest_cc_size(ugraph)]
    for attacked_node in attack_order:
        neighbours = ugraph.pop(attacked_node)
        for neigh in neighbours:
            ugraph[neigh].remove(attacked_node)
        resiliencies.append(largest_cc_size(ugraph))
    return resiliencies   
    
def compute_resilience_uf(ugraph, attack_order):
    """
    returns a list of largest connected componemts remaining after successive removing
    a list of attack_order "servers" from the network
    """
    servers = set()
    for node in ugraph:
        servers.add(node)
    attacked_servers = set()
    for node in attack_order:
        attacked_servers.add(node)
    working_servers = servers.difference(attacked_servers)
    network = UF(servers)
    resiliences = []
    if working_servers:
        for server1 in working_servers:
            for server2 in ugraph[server1]:
                if server2 in working_servers:
                    network.union(server1, server2)
        resiliences.append(network.get_resilience())
    else:
        resiliences.append(0)
    order = deque()
    for elem in attack_order:
        order.appendleft(elem)
    for server in order:
        working_servers.add(server)
        neighbours = ugraph[server]
        for node in neighbours:
            if node in working_servers:
                network.union(server, node)
        resiliences.append(network.get_resilience())    
    resiliences.reverse()    
    return resiliences  
    
def get_number_of_edges(graph):
    edges = 0
    for node in graph:
        for neigh in graph[node]:
            edges = edges + 1
    return edges/2
    
def get_p(graph):
    nodes = len(graph)
    edges = get_number_of_edges(graph)
    step = 0.25
    p = 0.5
    tol = 10
    while True:
        ER_graph = ER(nodes, p)
        edges_num = get_number_of_edges(ER_graph)
        if abs(edges_num - edges) < tol:
            break
        if edges_num < edges:
            p = p + step
        else:
            p = p - step
        step = step/2  
    return p    
    
def random_order(graph):
    nodes = []
    for node in graph:
        nodes.append(node)
    rand_order = []
    length = len(nodes)
    for i in range(0, length):
        random_node = random.choice(nodes)
        rand_order.append(random_node)
        nodes.remove(random_node)
    return rand_order   
    
def targeted_order(ugraph):
    """
    Compute a targeted attack order consisting
    of nodes of maximal degree
    Returns:
    A list of nodes
    """
    # copy the graph
    new_graph = copy_graph(ugraph)
    order = []    
    while len(new_graph) > 0:
        max_degree = -1
        for node in new_graph:
            if len(new_graph[node]) > max_degree:
                max_degree = len(new_graph[node])
                max_degree_node = node
        
        neighbors = new_graph[max_degree_node]
        new_graph.pop(max_degree_node)
        for neighbor in neighbors:
            new_graph[neighbor].remove(max_degree_node)
        order.append(max_degree_node)
    return order   
    
def fast_targeted_order(ugraph):  
    ugraph = copy_graph(ugraph)
    degree_sets = {}
    N = len(ugraph)
    for k in range(0, N):
        degree_sets[k] = set()
    for node in ugraph:
        degree = len(ugraph[node])
        degree_sets[degree].add(node)
    L = []
    for k in range(N-1, -1, -1):
        while degree_sets[k]:
            node = degree_sets[k].pop()
            neighbours = ugraph[node]
            for neigh in neighbours:
                degree = len(ugraph[neigh])
                degree_sets[degree].remove(neigh)
                degree_sets[degree-1].add(neigh)
            L.append(node)
            ugraph.pop(node)
            for neigh in neighbours:
                ugraph[neigh].remove(node)
    return L    

def target_order_timing():
    m = 5
    x = range(10, 1000, 10)
    slow_times = []
    fast_times = []
    for n in x:
        graph = UPA(n, m)
        begin = time.time()
        targeted_order(graph)
        slow_times.append(time.time() - begin)
        begin = time.time()
        fast_targeted_order(graph)
        fast_times.append(time.time() - begin)
    plt.figure()    
    plt.xlabel('number of nodes')
    plt.ylabel('time in seconds')
    plt.plot(x, slow_times, '-b', label='targeted_order')
    plt.plot(x, fast_times, '-r', label='fast_targeted_order')
    plt.title('runing time analysis in Desktop Anaconda Python')
    plt.legend(loc='upper right')
    plt.show()            
    
def make_plot(network_resilience, ER_resilience, UPA_resilience, N, title):
    x = range(0, N+1)
    plt.figure()
    plt.plot(x, network_resilience, '-r', label='network')
    plt.plot(x, ER_resilience, '-b', label='ER with p = ' + str(p))
    plt.plot(x, UPA_resilience, '-m', label='UPA with m = ' + str(m))
    plt.legend(loc='upper right')
    plt.xlabel('nodes removed number')
    plt.ylabel('resilience')
    plt.title(title)
    plt.show()           
        
def perform_random_attack(network, ER_graph, UPA_graph):
    random_network_attack = random_order(network)
    random_ER_attack = random_order(ER_graph)
    random_UPA_attack = random_order(UPA_graph)
    network_resilience = compute_resilience_uf(network, random_network_attack)
    ER_resilience = compute_resilience_uf(ER_graph, random_ER_attack)
    UPA_resilience = compute_resilience_uf(UPA_graph, random_UPA_attack)
    make_plot(network_resilience, ER_resilience, UPA_resilience, len(network), "random attack exploration")
    
def perform_targeted_attack(network, ER_graph, UPA_graph):
    targeted_network_attack = fast_targeted_order(network)
    targeted_ER_attack = fast_targeted_order(ER_graph)
    targeted_UPA_attack = fast_targeted_order(UPA_graph)
    network_resilience = compute_resilience_uf(network, targeted_network_attack)
    ER_resilience = compute_resilience_uf(ER_graph, targeted_ER_attack)
    UPA_resilience = compute_resilience_uf(UPA_graph, targeted_UPA_attack)
    make_plot(network_resilience, ER_resilience, UPA_resilience, len(network), "targeted attack exploration")    
     
if __name__ == "__main__":
    p = 0.0017
    m = 3
    network = load_graph(NETWORK_URL)
    ER_graph = ER(len(network), p)
    UPA_graph = UPA(len(network), m)
    perform_random_attack(network, ER_graph, UPA_graph)
    perform_targeted_attack(network, ER_graph, UPA_graph)
    target_order_timing()