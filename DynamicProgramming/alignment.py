"""
implements a dynamic programming solution to the sequence alignment problem
"""

import load_code
import time
import random
import matplotlib.pyplot as plt
import math


def build_scoring_matrix(alphabet, diag_score, off_diag_score, dash_score):
    """
    returns a scoring matrix
    """
    
    scores = {}
    for letter1 in alphabet:
        scores[letter1] = {}
        for letter2 in alphabet:
            if letter1 == '-' or letter2 == '-':
                scores[letter1][letter2] = dash_score
            elif letter1 == letter2:
                scores[letter1][letter2] = diag_score
            else:
                scores[letter1][letter2] = off_diag_score
    return scores                
   
             
def compute_alignment_matrix(seq_x, seq_y, scoring_matrix, global_flag):
    """
    returns an alignment matrix
    """            
    
    len_x = len(seq_x)
    len_y = len(seq_y)
    alignment_matrix = [[] for idx in range(len_x+1)]
    alignment_matrix[0].append(0)
    for idx in range(1, len_x+1):
        if global_flag:
            alignment_matrix[idx].append(alignment_matrix[idx-1][0] + scoring_matrix[seq_x[idx-1]]['-'])
        else:
            alignment_matrix[idx].append(max(0, alignment_matrix[idx-1][0] + scoring_matrix[seq_x[idx-1]]['-']))
    for idy in range(1, len_y+1):
        if global_flag:
            alignment_matrix[0].append(alignment_matrix[0][idy-1] + scoring_matrix['-'][seq_y[idy-1]])
        else:
            alignment_matrix[0].append(max(0, alignment_matrix[0][idy-1] + scoring_matrix['-'][seq_y[idy-1]]))
    for idx in range(1, len_x+1):
        for idy in range(1, len_y+1):
            scores = [alignment_matrix[idx-1][idy] + scoring_matrix[seq_x[idx-1]]['-'], alignment_matrix[idx][idy-1] + scoring_matrix['-'][seq_y[idy-1]], alignment_matrix[idx-1][idy-1] + scoring_matrix[seq_x[idx-1]][seq_y[idy-1]]]
            if global_flag:            
                alignment_matrix[idx].append(max(scores))
            else:
                alignment_matrix[idx].append(max(0, max(scores)))   
    return alignment_matrix


def compute_global_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix): 
    """
    traceback on dynamic programming table
    """
    
    idx = len(seq_x)
    idy = len(seq_y)
    seq_x_prime = ""
    seq_y_prime = ""
    score = alignment_matrix[idx][idy]
    while idx != 0 and idy != 0:
        if alignment_matrix[idx][idy] == alignment_matrix[idx-1][idy-1] + scoring_matrix[seq_x[idx-1]][seq_y[idy-1]]:
            seq_x_prime = seq_x[idx-1] + seq_x_prime
            seq_y_prime = seq_y[idy-1] + seq_y_prime
            idx -= 1
            idy -= 1
        elif alignment_matrix[idx][idy] == alignment_matrix[idx-1][idy] + scoring_matrix[seq_x[idx-1]]['-']:
            seq_x_prime = seq_x[idx-1] + seq_x_prime
            seq_y_prime = '-' + seq_y_prime
            idx -= 1
        else:
            seq_x_prime = '-' + seq_x_prime
            seq_y_prime = seq_y[idy-1] + seq_y_prime
            idy -= 1
    while idx != 0:
        seq_x_prime = seq_x[idx-1] + seq_x_prime 
        seq_y_prime = '-' + seq_y_prime
        idx -= 1
    while idy != 0:
        seq_y_prime = seq_y[idy-1] + seq_y_prime  
        seq_x_prime = '-' + seq_x_prime
        idy -= 1
    return (score, seq_x_prime, seq_y_prime)    
            

def compute_local_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix):
    """
    search for a local alignment procedure
    """
    
    idx = -1
    idy = -1
    max_so_far = -1
    for row in range(len(alignment_matrix)):
        for column in range(len(alignment_matrix[0])):
            if max_so_far < alignment_matrix[row][column]:
                max_so_far = alignment_matrix[row][column]
                idx = row
                idy = column            
    seq_x_prime = ""
    seq_y_prime = ""
    score = alignment_matrix[idx][idy]
    while alignment_matrix[idx][idy] != 0:
        if alignment_matrix[idx][idy] == alignment_matrix[idx-1][idy-1] + scoring_matrix[seq_x[idx-1]][seq_y[idy-1]]:
            seq_x_prime = seq_x[idx-1] + seq_x_prime
            seq_y_prime = seq_y[idy-1] + seq_y_prime
            idx -= 1
            idy -= 1
        elif alignment_matrix[idx][idy] == alignment_matrix[idx-1][idy] + scoring_matrix[seq_x[idx-1]]['-']:
            seq_x_prime = seq_x[idx-1] + seq_x_prime
            seq_y_prime = '-' + seq_y_prime
            idx -= 1
        else:
            seq_x_prime = '-' + seq_x_prime
            seq_y_prime = seq_y[idy-1] + seq_y_prime
            idy -= 1
    return (score, seq_x_prime, seq_y_prime)    


def compute_edit_distance(word1, word2, scoring_matrix):
    """
    returns an edit distance between two strings    
    """
    
    edit_dist = len(word1) + len(word2) - compute_alignment_matrix(word1, word2, scoring_matrix, True)[-1][-1]
    return edit_dist
    
    
def check_spelling(checked_word, dist, word_list, scoring_matrix):
    """
    returns a set of words that is similar to checked word (proposed spell corrections)    
    """
    
    words_within_dist = set()
    for word in word_list:
        edit_dist = compute_edit_distance(word, checked_word, scoring_matrix)
        if edit_dist <= dist:
            words_within_dist.add(word)
    return words_within_dist        
    
    
def scoring_matrix_for_edit_distance():
    """
    returns a scoring matrix that guide an edit distance calculations    
    """
    
    alphabet = "-abcdefghijklmnopqrstuvwxyz"
    diag_score = 2
    off_diag_score = 1
    dash_score = 0
    return build_scoring_matrix(alphabet, diag_score, off_diag_score, dash_score)  
 
 
def spell_check():
    """
    spell check task
    """
    
    scoring_matrix = scoring_matrix_for_edit_distance()
    word_list = load_code.read_words(load_code.WORD_LIST_URL)
    print check_spelling("humble", 1, word_list, scoring_matrix)
    print check_spelling("firefly", 2, word_list, scoring_matrix)
  
    
def eyeless_alignment():
    """
    returns an alignment of human and fruit fly eyeless gene
    """    
    
    scoring_matrix = load_code.read_scoring_matrix(load_code.PAM50_URL)
    HUMAN_EYELESS = load_code.read_protein(load_code.HUMAN_EYELESS_URL)
    FRUITFLY_EYELESS = load_code.read_protein(load_code.FRUITFLY_EYELESS_URL)
    alignment_matrix = compute_alignment_matrix(HUMAN_EYELESS, FRUITFLY_EYELESS, scoring_matrix, False)
    alignment = compute_local_alignment(HUMAN_EYELESS, FRUITFLY_EYELESS, scoring_matrix, alignment_matrix) 
    return alignment


def get_similarity(seq1, seq2):
    """
    returns persentage of identical corresponding letters
    """
    count = 0
    for idx in range(len(seq1)):
        if seq1[idx] == seq2[idx]:
            count += 1
    return (count + 0.0)/len(seq1)       

    
def consensusPAX_similarity():
    """
    PAX domain determining
    """
    
    human_PAX =    'HSGVNQLGGVFVNGRPLPDSTRQKIVELAHSGARPCDISRILQVSNGCVSKILGRYYETGSIRPRAIGGSKPRVATPEVVSKIAQYKRECPSIFAWEIRDRLLSEGVCTNDNIPSVSSINRVLRNLASEKQQ' 
    fruitfly_PAX = 'HSGVNQLGGVFVGGRPLPDSTRQKIVELAHSGARPCDISRILQVSNGCVSKILGRYYETGSIRPRAIGGSKPRVATAEVVSKISQYKRECPSIFAWEIRDRLLQENVCTNDNIPSVSSINRVLRNLAAQKEQQ'
    CONSENSUS_PAX = load_code.read_protein(load_code.CONSENSUS_PAX_URL)
    scoring_matrix = load_code.read_scoring_matrix(load_code.PAM50_URL)
    alignment_matrix_human = compute_alignment_matrix(human_PAX, CONSENSUS_PAX, scoring_matrix, True)
    alignment_matrix_fruitfly = compute_alignment_matrix(fruitfly_PAX, CONSENSUS_PAX, scoring_matrix, True)
    alignment_human = compute_global_alignment(human_PAX, CONSENSUS_PAX, scoring_matrix, alignment_matrix_human) 
    alignment_fruitfly = compute_global_alignment(fruitfly_PAX, CONSENSUS_PAX, scoring_matrix, alignment_matrix_fruitfly)
    print "Similarity of human and PAX: ", get_similarity(alignment_human[1], alignment_human[2])
    print "Similarity of fruitfly and PAX: ", get_similarity(alignment_fruitfly[1], alignment_fruitfly[2])
    
    
def generate_null_distribution(seq_x, seq_y, scoring_matrix, num_trials):
    """
    returns a scoring distribution
    """    
    
    scoring_distribution = {}
    count = 0
    for trial in range(num_trials):
        l = list(seq_y)
        random.shuffle(l)
        rand_y = ''.join(l)
        alignment_matrix = compute_alignment_matrix(seq_x, rand_y, scoring_matrix, False)
        score = compute_local_alignment(seq_x, rand_y, scoring_matrix, alignment_matrix)[0]
        if scoring_distribution.has_key(score):
            scoring_distribution[score] += 1
        else:
            scoring_distribution[score] = 1
        if count%10 == 0:
            print count
        count += 1    
    return scoring_distribution
    

def hypothesys_testing():
    """
    perform a hypothesys testing    
    """
    
    scoring_matrix = load_code.read_scoring_matrix(load_code.PAM50_URL)
    HUMAN_EYELESS = load_code.read_protein(load_code.HUMAN_EYELESS_URL)
    FRUITFLY_EYELESS = load_code.read_protein(load_code.FRUITFLY_EYELESS_URL)
    num_trials = 1000
    scoring_distribution = generate_null_distribution(HUMAN_EYELESS, FRUITFLY_EYELESS, scoring_matrix, num_trials)
    scores = scoring_distribution.keys()
    scores.sort()
    counts = [(scoring_distribution[score]+0.0)/num_trials for score in scores]
    plt.bar(scores, counts)
    plt.xlabel('Scores')
    plt.ylabel('Probability estimate')
    plt.title('Probability distribution estimate of different score values')
    mean = 0.0
    count = 0
    for score in scores:
        mean += score*scoring_distribution[score]
        count += scoring_distribution[score]
    mean = mean/count   
    sigma = 0.0
    for score in scores:
        sigma += scoring_distribution[score]*(score - mean)**2
    sigma = math.sqrt(sigma/count)  
    alignment_matrix = compute_alignment_matrix(HUMAN_EYELESS, FRUITFLY_EYELESS, scoring_matrix, False)
    alignment_score = compute_local_alignment(HUMAN_EYELESS, FRUITFLY_EYELESS, scoring_matrix, alignment_matrix)[0]
    z_score = (alignment_score - mean)/sigma
    print "Mean = ", mean, " Sigma = ", sigma, " z-score = ", z_score
    
if __name__ == "__main__":
    begin = time.time()
    #spell_check()
    #eyeless_human_fruitfly_alignment = eyeless_alignment()
    #print eyeless_human_fruitfly_alignment
    #consensusPAX_similarity()
    hypothesys_testing()
    print "Timing results: ", time.time() - begin, " seconds"
