"""
Application portion of Module 1

"""

# general imports
import urllib2
import matplotlib.pyplot as plt
from degree_distribution import *
import random
from DPATrial import *

# Set timeout for CodeSkulptor if necessary
#import codeskulptor
#codeskulptor.set_timeout(20)


###################################
# Code for loading citation graph

CITATION_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_phys-cite.txt"

def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph
    
    Returns a dictionary that models a graph
    """
    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[ : -1]
    
    print "Loaded graph with", len(graph_lines), "nodes"
    
    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1 : -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph
    
def ER(n, p):
    """
    generates a random graph
    
    """
    random_digraph = {}
    for i in range(0, n):
        random_digraph[i] = set([])
        for j in range(0, n):
            if i != j and random.random() < p:
                random_digraph[i].add(j)
    return random_digraph            
    
def DPA(n, m):  
    """
    generates a hierarchical "random graph"
    
    """
    graph = make_complete_graph(m)
    trial = DPATrial(m)
    for i in range(m, n):
        neigh = trial.run_trial(m)
        graph[i] = neigh
    return graph  
    
def make_loglog_plot(degree_distrib, format_str, name_of_graph):
    """
    generates log log plot of graph's degree distribution     
        
    """
    degrees = []
    node_nums = []
    for deg in degree_distrib:
        if deg != 0:
            degrees.append(deg)
            node_nums.append(degree_distrib[deg])
    
    norm_const = sum(node_nums)
    for i in range(0, len(node_nums)):
        node_nums[i] = (node_nums[i] + 0.0)/norm_const 
    plt.figure()
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('degree')
    plt.ylabel('fraction of nodes')
    plt.title('log-log plot of degree distribution of ' + name_of_graph)
    plt.plot(degrees, node_nums, format_str)
    plt.show() 

def average_out_degree(digraph):
    """
    computes average out-degree of the digraph
    
    """
    acc = 0
    node_num = 0
    for node in digraph:
        acc += len(digraph[node])
        node_num += 1
    return (acc + 0.0)/node_num    

def main():       
    #citation_graph = load_graph(CITATION_URL)
    #citation_degree_distrib = in_degree_distribution(citation_graph)
    #make_loglog_plot(citation_degree_distrib, 'ro', 'citation graph') 
    #n = 1000
    #p = 0.3
    #rand_graph = ER(n, p)
    #random_distrib = in_degree_distribution(rand_graph)
    #make_loglog_plot(random_distrib, 'bo', 'ER(' + str(n) + ', ' + str(p) + ') graph')
    #print average_out_degree(citation_graph)
    n = 27770
    m = 13
    DPAGraph = DPA(n, m)
    DPA_distrib = in_degree_distribution(DPAGraph)
    make_loglog_plot(DPA_distrib, 'bo', 'DPA(' + str(n) + ', ' + str(m) + ')  Graph')
    
if __name__ == "__main__":       
    main()    
        