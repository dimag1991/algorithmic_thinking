"""
computes in-degree distribution of digraph 
"""

EX_GRAPH0 = {0 : set([1, 2]),
             1 : set(),
             2 : set()}
EX_GRAPH1 = {0 : set([1, 4, 5]),
             1 : set([2, 6]),
             2 : set([3]),
             3 : set([0]),
             4 : set([1]),
             5 : set([2]),
             6 : set()}
EX_GRAPH2 = {0 : set([1, 4, 5]),
             1 : set([2, 6]),
             2 : set([3, 7]),
             3 : set([7]),
             4 : set([1]),
             5 : set([2]),
             6 : set(),
             7 : set([3]),
             8 : set([1, 2]),
             9 : set([0, 3, 4, 5, 6, 7])}

def make_complete_graph(num_nodes):
    """
    returns a complete graph
    """
    graph = {}
    for node in range(0, num_nodes):
        graph[node] = set()
        for neigh in range(0, num_nodes):
            if node != neigh:
                graph[node].add(neigh)
    return graph

def compute_in_degrees(digraph):
    """
    returns a dictionary of node's degrees
    """
    degrees = {}
    for node in digraph:
        degrees[node] = 0
    for node in digraph:
        for neigh in digraph[node]:
            degrees[neigh] += 1
    return degrees

def in_degree_distribution(digraph):
    """
    returns a dictionary of degree distribution on given digraph
    """
    distrib = {}
    degrees = compute_in_degrees(digraph)
    for node in degrees:
        degree = degrees[node]
        if distrib.has_key(degree):
            distrib[degree] += 1
        else:
            distrib[degree] = 1
    return distrib
